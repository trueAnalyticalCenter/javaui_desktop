# README #



### What is this repository for? ###

UI for desktop computers

### How do I get set up? ###

Need to install JVM 
![Снимок экрана 2016-10-25 в 20.07.25.png](https://bitbucket.org/repo/ajoo7R/images/1451349412-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202016-10-25%20%D0%B2%2020.07.25.png)

or you may look here:
http://ru.wikihow.com/%D0%B7%D0%B0%D0%BF%D1%83%D1%81%D1%82%D0%B8%D1%82%D1%8C-.JAR-%D1%84%D0%B0%D0%B9%D0%BB

And run application from this link: 
https://drive.google.com/file/d/0B8hOOkVtEM0eMlVkYm9ZeDhsX0E/view?usp=sharing

### Contribution guidelines ###

 This application is designed to obtain information about the individual rankings on various sites in the given amount of time. 


### What do you use? ###

We use swing here and possibly on the newest version we`d have JavaFX


### Who do I talk to? ###

Slack (@dredintellekt) or you may send me msg here

### Application scenario

![Снимок экрана 2016-10-21 в 20.24.50.png](https://bitbucket.org/repo/ajoo7R/images/3557110143-%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0%202016-10-21%20%D0%B2%2020.24.50.png)