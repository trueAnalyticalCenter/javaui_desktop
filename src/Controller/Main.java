package Controller;

import View.Statistics;

import java.awt.*;

/**
 * Created by Dred on 28.09.2016.
 */
public class Main {
    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {
            Statistics statistics = new Statistics();
            statistics.setVisible(true);
        });
 

    }
}
