package View;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Dred on 28.09.2016.
 */
public class FullStatic {
    private JPanel staticTable;
    private JScrollPane scrollPane;
    private JTable table;
    private long[] hits = {100500, 500, 100}; //берем значение упоминаний из SQL
    private String[] columnNames = {"Личность", "Количество упоминаний"};
    private String[][] personsAndHits =
            {{"Путин", "100500"},
                    {"Медведев", "500"},
                    {"Навальный", "100"}};

    public JPanel FullStaticTable() {
        staticTable = new JPanel();
        table = new JTable(personsAndHits, columnNames);
        scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setPreferredSize(new Dimension(300, 200));
        staticTable.add(scrollPane);
        return staticTable;
    }
    //Необходимо организовать запросы из SQL
    //Временно фейковые значения
    // https://www.youtube.com/watch?v=MSccUn5gS3M собственно как это сделать



   /* public JScrollPane createTable() {

        table = new JTable(personsAndHits, columnNames);
        scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setPreferredSize(new Dimension(300, 200));
        return scrollPane;
    }*/


}
