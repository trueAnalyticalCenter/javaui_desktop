package View;

import javafx.geometry.VerticalDirection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Statistics extends JFrame implements ActionListener {

    private final Font fontBold = new Font("Italic", Font.BOLD, 14);
    private final Font fontDefault = new Font("Times New Roman", Font.ITALIC, 14);
    private final int buttonCount = 6;
    String title = "AnyStat";
    EverydayStatic eStat = new EverydayStatic();
    FullStatic fStat = new FullStatic();
    Panels panels = new Panels();
    JPanel buttonPanel;
    private String[] buttonName = {"Общая статистика", "Ежедневная статистика", "Справочники", "Личности", "Ключевые слова", "Сайты"};
    private ImageIcon image;
    private JPanel rightPanel;
    private JPanel commonPanel;
    private JPanel leftPanel;
    private JButton[] buttons;


    private JPanel fullStat;
    private JPanel everydayStat;
    private JPanel personBox;
    private JPanel siteBox;
    private JPanel dateBox;
    private String btnTextFlag;


    public Statistics() {
        initUI();
    }

    private void initUI() {

        setTitle(title);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(600, 500);
        setLocationRelativeTo(null);


        //Изменение изображения иконки самого окна
        image = new ImageIcon("lib/AnyStat1.png");
        this.setIconImage(image.getImage());

        commonPanel = new JPanel(new FlowLayout());
        leftPanel = new JPanel();
        rightPanel = new JPanel();

        // Панельки с panels
        fullStat = new JPanel();
        fullStat = fStat.FullStaticTable();
        personBox = panels.personPanel();
        dateBox = panels.datePanel();
        siteBox = panels.sitePanel();

        //Делаем левую сторону
        leftPanel.setLayout(new FlowLayout());
        leftPanel.add(createButtons());
        leftPanel.setBorder(BorderFactory.createLineBorder(Color.RED));

        //Делаем правую сторону
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
        //rightPanel.setLayout(new FlowLayout());
        rightPanel.add(siteBox);
        rightPanel.add(fullStat);// Добавляем общую. статистику

        //todo делаем высоту и ширину левой панели аналогично правой. Надо получать как-то только высоту. Надо разобраться
        //leftPanel.setPreferredSize(rightPanel.getPreferredSize());


        //добавляем все на главное окно
        commonPanel.add(leftPanel);
        commonPanel.add(rightPanel);

        add(commonPanel);

        pack();

    }


    //Надо сделать кнопки единого размера
    protected JComponent createButtons() {

        buttonPanel = new JPanel();
        buttons = new JButton[buttonCount];

        for (int i = 0; i < buttonCount; i++) {

            JButton button = new JButton();
            buttons[i] = button;
            buttons[i].setText(buttonName[i]);

            //убираем графику для кнопки
            setButtonGraphic(buttons[i]);
            if (buttons[i].getText().contains("Справочники"))
            {

                //buttons[i].setFocusable(false);
                //buttons[i].setEnabled(false);
            }

            //добавляем кнопки на панель
            buttonPanel.add(button);
            button.addActionListener(this);
        }

        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
//        buttonPanel.setBorder(new EmptyBorder(new Insets(40, 60, 40, 60)));

        /*JButton mainStatistic = new JButton("Общая статистика");
        JButton dayStatistic = new JButton("Ежедневная статистика");
        JButton handbookButton = new JButton("Справочники");
        //дополнительные кнопки. Их надо будет сдвинуть на экране и сделать шрифт меншье
        JButton personButton = new JButton("Личности");
        JButton keywordsButton = new JButton("Ключевые слова");
        JButton sitesButton = new JButton("Сайты");


        mainStatistic.addActionListener(this);
        dayStatistic.addActionListener(this);
        handbookButton.addActionListener(this);
        personButton.addActionListener(this);
        keywordsButton.addActionListener(this);
        sitesButton.addActionListener(this);
        //buttonPanel.setMinimumSize(new Dimension(dayStatistic.getMaximumSize().width,100));
        dayStatistic.setOpaque(false);
        dayStatistic.setBorderPainted(false);
        dayStatistic.setBackground(Color.WHITE);

     *//*   mainStatistic.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });*//*

        //todo надо сделать это для всех кнопок, и также, надо добавить флаг, который будет проверять, нажали на другую кнопку или нет, чтобы выделение пропало
       *//* dayStatistic.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //JButton btn=new JButton();
                //System.out.println(e.getSource());

                dayStatistic.setFont(new Font("Italic", Font.BOLD, 14));
                dayStatistic.setForeground(Color.BLUE);
                dayStatistic.setText("<html><u>" + dayStatistic.getText() + "</u></html>");
                rightPanel.add(panels.sitePersonPanel());
            }
        });

*//*
        buttonPanel.add(mainStatistic);
        buttonPanel.add(dayStatistic);
        buttonPanel.add(handbookButton);
        buttonPanel.add(personButton);
        buttonPanel.add(keywordsButton);
        buttonPanel.add(sitesButton);

*/
        //Match the SpringLayout's gap, subtracting 5 to make
        //up for the default gap FlowLayout provides.
        buttonPanel.setBorder(BorderFactory.createLineBorder(Color.GREEN, 5));
        return buttonPanel;
    }

    private void setButtonGraphic(JButton button) {
        button.setOpaque(false);
        button.setBorderPainted(false);
        button.setBackground(Color.WHITE);
        button.setText("<html><u>" + button.getText() + "</u></html>");
        button.setForeground(Color.BLUE);
    }

    // сайт и название его, личность и кого выбрали, возможно не потребуется
    private void createRightLayout(JComponent... arg) {

        Container pane = getContentPane();
        GroupLayout gl = new GroupLayout(pane);
        pane.setLayout(gl);


        gl.setAutoCreateContainerGaps(true);
        gl.setAutoCreateGaps(true);

        gl.setHorizontalGroup(gl.createSequentialGroup()
                .addGroup(gl.createParallelGroup()
                        .addComponent(arg[1]).addGap(100)
                        .addComponent(arg[3]))
                .addGroup(gl.createParallelGroup()
                        .addComponent(arg[0])
                        .addComponent(arg[2]))
        );

        gl.setVerticalGroup(gl.createParallelGroup()
                .addGroup(gl.createSequentialGroup().addGap(5)
                        .addComponent(arg[1]).addGap(15)
                        .addComponent(arg[3]))
                .addGroup(gl.createSequentialGroup()
                        .addComponent(arg[0])
                        .addComponent(arg[2]))
        );

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton btn = (JButton) e.getSource();
        setDefaultFont();
        btnTextFlag = btn.getText();
        btn.setFont(fontBold);


//todo уточнить, что быстрее, отрисовка правой панели целиком, либо удалить объекты и добавить их снова в правую панель. Делаю пока второй вариант.
        if (btn.getText().contains("Ежедневная статистика")) {
            try {
                if (!rightPanel.isAncestorOf(personBox)) {
                    rightPanel.remove(fullStat);
                    rightPanel.add(personBox);
                    rightPanel.add(fullStat);
                }
            } catch (Exception e1) {
                System.err.println("Не получилось удалить fullStat");
            }
        }
        if (btn.getText().contains("Общая статистика")) {
            try {
                if (rightPanel.isAncestorOf(personBox)) {
                    rightPanel.remove(personBox);
                }
            } catch (Exception e1) {
                System.err.println("Не получилось удалить personBox");
            }

        }
        if (btn.getText().contains("Личности")) {

            //Что-то делать
        }
        try {
            //Что-то делать


        } catch (Exception e1) {
            System.err.println("Не получилось получить Справочники");
        }

        if (btn.getText().contains("Ключевые слова"))
            try {
                //Что-то делать


            } catch (Exception e1) {
                System.err.println("Не получилось получить Ключевые слова");
            }

        if (btn.getText().contains("Сайты"))
            try {
                //Что-то делать


            } catch (Exception e1) {
                System.err.println("Не получилось получить Сайты");
            }

        revalidate();
        repaint();
        pack();
    }


    public void setDefaultFont() {
        for (int i = 0; i < buttonCount; i++) {
            if (buttons[i].getText() == btnTextFlag) {
                buttons[i].setFont(fontDefault);
                break;
            }
        }

    }
}

