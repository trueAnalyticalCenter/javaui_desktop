package View;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;


public class Panels {
    private JPanel choosePanel;
    private JPanel onlySitePanel;
    private JPanel siteAndPersonPanel;
    private JPanel siteAndPersonAndDatePanel;
    private JComboBox<String> siteBox;
    private JComboBox<String> nameBox;
    private JLabel siteLabel;
    private JLabel nameLabel;
    private Border solidBorder;
    private String[] names;
    private String[] sites;


    private JPanel site;
    private JPanel date;
    private JPanel person;


    // реализация различных панелей Имя, сайт, дата


    public JPanel sitePanel() {

        sites = new String[]{"lenta.ru", "yandex.ru"}; // Нужно заменить сайты на значения, которые будут браться из SQL
        site = new JPanel(new GridLayout(1, 1));
        siteBox = new JComboBox(sites);
        siteLabel = new JLabel("Сайт: ");
        solidBorder = BorderFactory.createLineBorder(Color.BLACK, 1);
        site.add(siteLabel);
        site.add(siteBox);

        return site;
    }

    public JPanel personPanel() {
        names = new String[]{"Путин", "Медведев", "Навальный"};
        person = new JPanel(new GridLayout(1, 1));
        solidBorder = BorderFactory.createLineBorder(Color.BLACK, 1);
        nameBox = new JComboBox(names);
        nameLabel = new JLabel("Личность: ");
        person.add(nameLabel);
        person.add(nameBox);

        return person;
    }

    public JPanel datePanel() {

        return date;
    }


/*    public JPanel sitePanel() {

        onlySitePanel = new JPanel(new GridLayout(1, 1));
        solidBorder = BorderFactory.createLineBorder(Color.BLACK, 1);

        sites = new String[]{"lenta.ru", "yandex.ru"}; // Нужно заменить сайты на значения, которые будут браться из SQL
        siteBox = new JComboBox<>(sites);
        siteLabel = new JLabel("Сайт: ");
        onlySitePanel.add(siteLabel);
        onlySitePanel.add(siteBox);

        choosePanel.add(onlySitePanel);
        choosePanel.setBorder(solidBorder);

        return choosePanel;
    }

    public JPanel sitePersonPanel() {
        siteAndPersonPanel = new JPanel(new GridLayout(2, 2)); // 2 rows 1 column
        choosePanel = new JPanel();


        solidBorder = BorderFactory.createLineBorder(Color.BLACK, 1);

        names = new String[]{"Путин", "Медведев", "Навальный"};// Нужно заменить имена на значения, которые будут браться из SQL
        sites = new String[]{"lenta.ru", "yandex.ru"}; // Нужно заменить сайты на значения, которые будут браться из SQL

        nameBox = new JComboBox<>(names);
        siteBox = new JComboBox<>(sites);

        nameLabel = new JLabel("Личность: ");
        siteLabel = new JLabel("Сайт: ");

        siteAndPersonPanel.add(nameLabel);
        siteAndPersonPanel.add(nameBox);
        siteAndPersonPanel.add(siteLabel);
        siteAndPersonPanel.add(siteBox);


        choosePanel.add(siteAndPersonPanel);

        choosePanel.setBorder(solidBorder);

        return choosePanel;
    }

    public JPanel sitePersonDatePanel() {
        siteAndPersonAndDatePanel = new JPanel(new GridLayout(3, 3)); // 2 rows 1 column
        choosePanel = new JPanel();


        solidBorder = BorderFactory.createLineBorder(Color.BLACK, 1);

        names = new String[]{"Путин", "Медведев", "Навальный"};// Нужно заменить имена на значения, которые будут браться из SQL
        sites = new String[]{"lenta.ru", "yandex.ru"}; // Нужно заменить сайты на значения, которые будут браться из SQL

        nameBox = new JComboBox<>(names);
        siteBox = new JComboBox<>(sites);

        nameLabel = new JLabel("Личность: ");
        siteLabel = new JLabel("Сайт: ");

        siteAndPersonAndDatePanel.add(nameLabel);
        siteAndPersonAndDatePanel.add(nameBox);
        siteAndPersonAndDatePanel.add(siteLabel);
        siteAndPersonAndDatePanel.add(siteBox);


        choosePanel.add(siteAndPersonAndDatePanel);

        choosePanel.setBorder(solidBorder);

        return choosePanel;
    }*/
}
