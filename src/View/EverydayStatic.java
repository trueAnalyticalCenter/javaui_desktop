package View;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;


public class EverydayStatic {
    private Border solidBorder;
    private JPanel choosePanel;
    private JPanel labelPanel;
    private String[] names;
    private String[] sites;
    private JComboBox<String> nameBox;
    private JComboBox<String> siteBox;
    private JLabel siteLabel;
    private JLabel nameLabel;

    // сайт и название его, личность и кого выбрали, А еще надо добавить временной интервал, за который статистику брать.
    protected JPanel createChooseField() {

        choosePanel = new JPanel();
        labelPanel = new JPanel(new GridLayout(2, 2)); // 2 rows 1 column


        solidBorder = BorderFactory.createLineBorder(Color.BLACK, 1);

        names = new String[]{"Путин", "Медведев", "Навальный"};// Нужно заменить имена на значения, которые будут браться из SQL
        sites = new String[]{"lenta.ru", "yandex.ru"}; // Нужно заменить сайты на значения, которые будут браться из SQL

        nameBox = new JComboBox<>(names);
        siteBox = new JComboBox<>(sites);

        nameLabel = new JLabel("Личность: ");
        siteLabel = new JLabel("Сайт: ");

        labelPanel.add(nameLabel);
        labelPanel.add(nameBox);
        labelPanel.add(siteLabel);
        labelPanel.add(siteBox);


        choosePanel.add(labelPanel);

        choosePanel.setBorder(solidBorder);

        return choosePanel;
    }
}
